FROM python:3.9-alpine

ARG WORKDIR

COPY ./* /tmp/

RUN pip install -r /tmp/requirements.txt